'use strict';

const inherits = require('util').inherits;
const mkdirp = require('mkdirp');
const fs = require('fs');
const assert = require('assert');
var stream = require('readable-stream');

const StorageAdapter = require('../adapter');
const utils = require('../../utils');

/**
 * Implements a file system storage adapter interface
 * @extends {StorageAdapter}
 * @param {String} storageDirPath - Path to storage folder
 * @constructor
 * @license AGPL-3.0
 */
function FileSystemStorageAdapter(storageDirPath) {
  if (!(this instanceof FileSystemStorageAdapter)) {
    return new FileSystemStorageAdapter(storageDirPath);
  }

  this._validatePath(storageDirPath);

  this._path = storageDirPath;
}

inherits(FileSystemStorageAdapter, StorageAdapter);

/**
 * Validates the storage path supplied
 * @private
 */
FileSystemStorageAdapter.prototype._validatePath = function(storageDirPath) {
  if (!utils.existsSync(storageDirPath)) {
    mkdirp.sync(storageDirPath);
  }

  assert(utils.isDirectory(storageDirPath), 'Invalid directory path supplied');
};

/**
 * Implements the abstract {@link StorageAdapter#_get}
 * @private
 * @param {String} key
 * @param {Function} callback
 */
FileSystemStorageAdapter.prototype._get = function(key, callback) {
  try {
    const serializedItem = fs.readFileSync(`${this._path}/${key}`);
    const item = JSON.parse(serializedItem);

    item.shard = new stream.Readable({
      read: function() {
        this.push(item.shard);
      }
    });

    callback(null, item);
  } catch (error) {
    callback(error);
  }
};

/**
 * Implements the abstract {@link StorageAdapter#_peek}
 * @private
 * @param {String} key
 * @param {Function} callback
 */
FileSystemStorageAdapter.prototype._peek = function(key, callback) {
  try {
    const serializedItem = fs.readFileSync(`${this._path}/${key}`);
    const item = JSON.parse(serializedItem);

    callback(null, item);
  } catch (error) {
    callback(error);
  }
};

/**
 * Implements the abstract {@link StorageAdapter#_put}
 * @private
 * @param {String} key
 * @param {Object} item
 * @param {Function} callback
 */
FileSystemStorageAdapter.prototype._put = function(key, item, callback) {
  try {
    item.fskey = utils.rmd160(key, 'hex');

    fs.appendFileSync(`${this._path}/${key}`, JSON.stringify(item));
    callback(null);
  } catch (error) {
    callback(error);
  }
};

/**
 * Implements the abstract {@link StorageAdapter#_del}
 * @private
 * @param {String} key
 * @param {Function} callback
 */
FileSystemStorageAdapter.prototype._del = function(key, callback) {
  try {
    fs.unlinkSync(`${this._path}/${key}`);
    callback(null);
  } catch (error) {
    callback(error);
  }
};

/**
 * Implements the abstract {@link StorageAdapter#_flush}
 * @private
 * @param {Function} callback
 */
FileSystemStorageAdapter.prototype._flush = function(callback) {
  callback();
};

/**
 * Implements the abstract {@link StorageAdapter#_size}
 * @private
 * @param {String} [key]
 * @param {Function} callback
 */
FileSystemStorageAdapter.prototype._size = function(key, callback) {
  // TODO
};

/**
 * Implements the abstract {@link StorageAdapter#_keys}
 * @private
 * @returns {ReadableStream}
 */
FileSystemStorageAdapter.prototype._keys = function() {
  const keys = [];

  try {
    fs.readdirSync(this._path).forEach(file => {
      keys.push(file);
    });
  } catch (error) {
    throw (error);
  }

  return new stream.Readable({
    read: function() {
      this.push(keys.length ? keys.shift() : null);
    }
  });
};

module.exports = FileSystemStorageAdapter;