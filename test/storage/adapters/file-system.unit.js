'use strict';

const os = require('os');
const rimraf = require('rimraf');
const path = require('path');
const mkdirp = require('mkdirp');
const expect = require('chai').expect;
const stream = require('readable-stream');

const FileSystemStorageAdapter = require('../../../lib/storage/adapters/file-system');
const StorageItem = require('../../../lib/storage/item');
const utils = require('../../../lib/utils');
const Contract = require('../../../lib/contract');
const AuditStream = require('../../../lib/audit-tools/audit-stream');

const TMP_DIR = path.join(os.tmpdir(), 'STORJ_FILE_SYSTEM_ADAPTER_TEST');
const TEST_HASH = utils.rmd160('test');
const MISSING_HASH = '';
const TEST_ITEM = new StorageItem({
  hash: TEST_HASH,
  shard: Buffer.from('test')
});
const TEST_CONTRACT = new Contract();
const audit = new AuditStream(12);

let store = null;

function tmpdir() {
  return path.join(TMP_DIR, 'test-' + Date.now() + '.db');
}

describe('FileSystemStorageAdapter', function() {

  before(function() {
    if (utils.existsSync(TMP_DIR)) {
      rimraf.sync(TMP_DIR);
    }
    mkdirp.sync(TMP_DIR);
    audit.end(Buffer.from('test'));
    store = new FileSystemStorageAdapter(tmpdir());
  });

  describe('@constructor', function() {
    it('should create instance without the new keyword', function() {
      expect(
        FileSystemStorageAdapter(tmpdir())
      ).to.be.instanceOf(FileSystemStorageAdapter);
    });
  });

  describe('#_validatePath', function() {
    it('should not make a directory that already exists', function() {
      expect(function() {
        const tmp = tmpdir();
        mkdirp.sync(tmp);
        FileSystemStorageAdapter.prototype._validatePath(tmp);
      }).to.not.throw(Error);
    });
  });

  describe('#_put', function() {

    it('should store the item', function(done) {
      TEST_ITEM.contracts[TEST_HASH] = TEST_CONTRACT;
      TEST_ITEM.challenges[TEST_HASH] = audit.getPrivateRecord();
      TEST_ITEM.trees[TEST_HASH] = audit.getPublicRecord();
      store._put(TEST_HASH, TEST_ITEM, function(err) {
        expect(err).equal(null);
        done();
      });
    });
  });

  describe('#_get', function() {
    it('should return the stored item', function(done) {
      store._get(TEST_HASH, function(err, item) {
        expect(err).to.equal(null);
        expect(item).to.be.instanceOf(Object);
        done();
      });
    });

    it('should return the stored item with shard property being a stream', function(done) {
      store._get(TEST_HASH, function(err, item) {
        expect(err).to.equal(null);
        expect(item).to.be.instanceOf(Object);
        expect(item.shard).to.be.instanceOf(stream.Readable);
        done();
      });
    });

    it('should return error if the data is not found', function(done) {
      store._get(MISSING_HASH, function(err) {
        expect(err).to.be.instanceOf(Object);
        done();
      });
    });
  });

  describe('#_peek', function() {
    it('should return the stored item', function(done) {
      store._peek(TEST_HASH, function(err, item) {
        expect(err).to.equal(null);
        expect(item).to.be.instanceOf(Object);
        done();
      });
    });

    it('should return error if the data is not found', function(done) {
      store._peek(MISSING_HASH, function(err) {
        expect(err).to.be.instanceOf(Object);
        done();
      });
    });
  });

  describe('#_keys', function() {
    it('should stream all of the keys', function(done) {
      const keyStream = store._keys();

      keyStream.on('data', function(key) {
        expect(key.toString()).to.equal(
          '5e52fee47e6b070565f74372468cdc699de89107'
        );
      }).on('end', done);
    });
  });


  describe('#_del', function() {
    it('should delete the shard if it exists', function(done) {
      store._del(TEST_HASH, function(err) {
        expect(err).to.equal(null);
        done();
      });
    });

    it('should return error if del fails', function(done) {
      store._del(MISSING_HASH, function(err) {
        expect(err).to.be.instanceOf(Object);
        done();
      });
    });
  });

  describe('#_size', function() {
    // TODO
  });
});

after(function() {
  rimraf.sync(TMP_DIR);
});
